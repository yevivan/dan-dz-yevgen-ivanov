"use strict"

class Employee {
    constructor (name, age, salary) {
        this._name = name,
        this._age = age,
        this._salary = salary
    }
        get name () {
            return this._name
        }
        set name (value) {
        this._name = value
    }
        get age () {
            return this._age
    }
        set age (value) {
            this._age = value
    }
    get salary () {
        return this._salary
    }
    set salary (value) {
        this._salary = value
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang
    }
    get salary () {
        return this._salary*3
    }

    sayHi () {
        console.log("Hello my name is  " + `${this.name}` + "  and my salary is  " + `${this.salary}`);
    }
}

let programmer = new Programmer ("Maasei Shimov", 34, 1500, "ENG");
console.log(programmer);
console.log(programmer.salary);

let programmerJS = new Programmer ("Maasei Gotchev", 45, 3400, "FRA");

let programmerADA = new Programmer ("Kuk", 34, 4000, "Hindi")

let programmerBasic = new Programmer ("Monika", 18, 2000, "ESP")


console.log(programmerJS);
console.log(programmerJS.salary);
programmerJS.sayHi();
console.log(programmerADA);
console.log(programmerADA.salary);
console.log(programmerBasic);
console.log(programmerBasic.salary);











