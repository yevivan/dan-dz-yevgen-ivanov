import { getcardsReducer } from "./GetCardsReducer";
import { GET_CARDS } from "./actions";

const initialState = {
  cards: [],
};

describe("CheckOut form reducer works", () => {
  test("should return the initial state", () => {
    expect(getcardsReducer(undefined, { type: undefined })).toEqual(
      initialState
    );
  });

  test("should get card", () => {
    expect(
      getcardsReducer(initialState.cards, { type: GET_CARDS, payload: true })
    ).not.toHaveLength(0)
  });
});
