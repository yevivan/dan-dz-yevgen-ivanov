import checkOutfromReducer from "./checkOutFormReducer"
import {  SHOW_CHECK_OUT_FORM } from "../Check our form/actions";

const initialState = {
    checkOutIsopen: false,
    buyNowBtnIsopen: true,
  };

describe('CheckOut form reducer works', () => {

    test('should return the initial state', () => {
        expect(checkOutfromReducer(undefined, { type: undefined })).toEqual(initialState)
      })

      test('should change isOpen', () => {
        expect(checkOutfromReducer(initialState, { type: SHOW_CHECK_OUT_FORM, payload: true })).toEqual({
            checkOutIsopen: true,
            buyNowBtnIsopen: true,
         })
      })
 })