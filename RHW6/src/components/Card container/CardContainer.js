import CardItem from "../Card/CardItem";
import CardItemListStyle from "../Card/CardItem list_style";
import styles from "./CardContainer.module.scss";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { addToFavorites } from "../../store/Getting cards/ActionsCreator";
import {
  setAddTocartModalProps,
  toggleAddtocartModal,
} from "../../store/Add to cart/ActionsCreator";
import { useState } from "react";
import { Context } from "./context";
import Button from "../Button/Button";

const CardContainer = (props) => {
  const cards = useSelector((store) => store.getCards.cards);
  const { showOnlyFavorities } = props;
  const [cardsListstyle, setCardsListstyle] = useState(false);
  const changeCardsListstyle = () => setCardsListstyle((prev) => !prev);
  console.log(cardsListstyle);
  // Используется этот контейнер для вывода либо всех карточек, либо избранных. Пропс showOnlyFavorities для условия

  if (!showOnlyFavorities) {
    return (
      <div className={styles.cardsContainer}>
        <Button
          className={styles.switchToListStyleBtn}
          onClick={changeCardsListstyle}
        >
          Show items list
        </Button>
        <ul className={cardsListstyle ?  styles.list :styles.blocks}>
          {cards.map((card) => (
            <li key={card.id}>
              <Context.Provider
                value={{
                  addToFavorites,
                  setAddTocartModalProps,
                  toggleAddtocartModal,
                  id:card.id,
                  title: card.title,
                  description: card.description,
                  img: card.img,
                  isFavorite: card.isFavorite,
                  price: card.price,
                  thisCard: card,
                }}
              >
                {cardsListstyle ? <CardItemListStyle/>:<CardItem/> }
              </Context.Provider>
            </li>
          ))}
        </ul>
      </div>
    );
  } else {
    return (
     
        <div>
          <ul className={styles.blocks}>
            {cards.map((card) => {
              if (card.isFavorite === true) {
                return (
                  <li key={card.id}>
                          <Context.Provider
                value={{
                  addToFavorites,
                  setAddTocartModalProps,
                  toggleAddtocartModal,
                  id:card.id,
                  title: card.title,
                  description: card.description,
                  img: card.img,
                  isFavorite: card.isFavorite,
                  price: card.price,
                  thisCard: card,
                }}
              >
                    <CardItem/> 
                    </Context.Provider>
                  </li>
                );
              }
            })}
          </ul>
        </div>

    );
  }
};

export default CardContainer;

CardContainer.propTypes = {
  showOnlyFavorities: PropTypes.bool,
  addToCart: PropTypes.func,
  cards: PropTypes.array,
  addToFavorites: PropTypes.func,
};

CardContainer.defaultProps = {
  cards: undefined,
};
