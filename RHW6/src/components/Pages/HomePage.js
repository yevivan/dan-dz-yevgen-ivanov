import CardContainer from "../Card container/CardContainer";
import ModalAddtocart from "../Modal/ModalAddTocart";
import styles from "../Pages/Pages.module.scss";
// import Button from "../Button/Button";

const HomePage = () => {
  return (
    <>
      <main className={styles.pageContainer}>
        {/* <section className={styles.leftContainer}> */}
          <h1>Available items</h1>
          {/* <Button  className={styles.switchToListstile}>Show items list</Button> */}
          <CardContainer />
        {/* </section> */}
      </main>
      <ModalAddtocart />
    </>
  );
};

export default HomePage;
