import React from "react";
import styles from './button.module.scss';



export default class Button extends React.PureComponent {
    render() {
        const { backgroundColor, handleClick, text } = this.props;
        return <button className = {styles.btn} style = {{backgroundColor}} onClick = {handleClick}  >
       {text}
        </button>
    }
}