import React from "react";
import styles from "./modal.module.scss";

export default class Modal extends React.Component {
    render() {
        const { modalTextcontent, action, isModalOpen, handleClickOnModalcloseBtn, skinColor} = this.props
 return <>
            <div className={styles.darkBG} onClick={handleClickOnModalcloseBtn} />
            <div className={styles.centered}>
                <div className={skinColor === "blue"? styles.modalBlue : styles.modal}>
                    <div className={skinColor === "blue"? styles.modalHeaderBlue : styles.modalHeader }>
                 <h5 className={styles.heading}>Make your choice</h5>
                 {isModalOpen &&    <button className={styles.closeBtn} onClick={handleClickOnModalcloseBtn}>X
                            </button>}
                         
                    </div>     
                    <div className={styles.modalContent} >
                            {modalTextcontent}
                        <div className={styles.modalActions} >{action}</div>
                    </div>
                </div>
            </div>
    
        </>

    }
}