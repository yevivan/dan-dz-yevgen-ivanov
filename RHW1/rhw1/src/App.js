
import './App.scss';
import React from 'react';
import Button from "./components/Button/button";
import Modal from './components/Modal/Modal'
import styles from './components/Modal/modal.module.scss'

export default class App extends React.Component {

  state = {
  isFirstModalOpen: false,
  isSecondModalOpen: false,
  }
   
   
  handleClickFirstModalBtn = () => {
    // alert(this.state.isFirstModalOpen)
    !this.state.isFirstModalOpen ? this.setState({ isFirstModalOpen: true }) : this.setState({ isFirstModalOpen: false })  
  }
 handleClickSecondModalBtn = () => {
    !this.state.isSecondModalOpen ? this.setState({ isSecondModalOpen: true }) : this.setState({ isSecondModalOpen: false })  
 }

  render() {  
    const { isFirstModalOpen } = this.state; 
    const { isSecondModalOpen } = this.state; 
    console.log(isSecondModalOpen);
    return <>
      <div style={{display: 'flex', width: "100%", height: 200, position: 'relative', top: "70vh", justifyContent: "space-between", marginLeft: 70}}>
            <Button backgroundColor = 'blue' handleClick = {this.handleClickFirstModalBtn} text = 'Blue pill'></Button>
            <Button backgroundColor='red' handleClick = {this.handleClickSecondModalBtn} text='Red pill'></Button>
            </div>
      
      {isFirstModalOpen &&
        <Modal modalTextcontent='Do you want to remain in contented ignorance with the blue pill?'
          isModalOpen={isFirstModalOpen}
          handleClickOnModalcloseBtn={this.handleClickFirstModalBtn}
          skinColor = {"blue"}
                action={ 
                            <div className={styles.actionsContainer}>
                                <button className={styles.deleteBtnBlue}  onClick = {this.handleClickFirstModalBtn}>Take</button>
                                <button className={styles.cancelBtnBlue} onClick = {this.handleClickFirstModalBtn}>Decline</button>
                            </div>
                        }>
        </Modal>
      }

        {isSecondModalOpen &&
          <Modal modalTextcontent='Do you want learn a potentially unsettling or life-changing truth by taking the red pill?'
            isModalOpen={isSecondModalOpen}
            handleClickOnModalcloseBtn = {this.handleClickSecondModalBtn}
                  action={
                              <div className={styles.actionsContainer}>
                                  <button className={styles.deleteBtn}  onClick = {this.handleClickSecondModalBtn}>Take</button>
                                  <button className={styles.cancelBtn} onClick = {this.handleClickSecondModalBtn}>Decline</button>
                              </div>
                          }>
          </Modal>
      }
          </>
  } 
}








// export default class App extends React.c {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
