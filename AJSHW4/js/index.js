"use strict"
const link = "https://ajax.test-danit.com/api/swapi/films"
const cardsContainer = document.querySelector("#films-list");


fetch  (link)
    .then ((response) => {
        return response.json() 
    })
    .then ((data) => {
        console.log(data);
        data.forEach(({ episodeId, name, openingCrawl})   => {
              cardsContainer.insertAdjacentHTML("beforeend", `<div id="container">
              <h2 id="film-name">${name}</h2>
              <span class="loader loader${episodeId} "></span>
              <div id="episode-number${episodeId}" class="episode">Episode ${episodeId}</div>
              <p id="openingCrawl">${openingCrawl}</p>
              <div id="characters-list">Characters</div>
              <ul id="actors-list${episodeId}">Characters</ul>
              </div>`);
        });
        return data.map(({episodeId, characters}) => 
        ({episodeId, characters}))
    }) 
    .then ((character) => {
                character.forEach(({episodeId, characters}) => {
              
                  characters.forEach((characterLink) => {
                
                      fetch(characterLink)
                      .then((response) => {
                      return response.json()
                      })
                      .then(({name}) => {
                        document.querySelector(`#actors-list${episodeId}`).insertAdjacentHTML("beforeend", `<li id="character">${name}</li>`)   
                      })
                      .then (() => {
                        document.querySelector(`.loader${episodeId}`).classList.add("hidden")
                      })
                      .catch((err2) => {
                        console.log(err2);
                      })
                  })

                })
                
    })
    .catch ((err) => {
      alert("Something went wrong!")
      console.log(err);
    })     
      



   





















// .then ((actors) => {
//     const actorsList = document.querySelectorAll("#actors-list")
//     actors.forEach(({characters}) => {
//         characters.forEach(el => {
//             fetch(el)
//             .then ((response) => {
//                 return response.json()
//             })
//             // .then (({name}) => console.log(name)); 
//         })
//         })
    
// })