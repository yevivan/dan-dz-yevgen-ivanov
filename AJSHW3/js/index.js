"use strict"

// Завдання 1

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

function mergeData (bataBase1, dataBase2) {
    let dataBase = [...bataBase1, ...dataBase2];
    dataBase =  dataBase.filter((item, index) => (dataBase.indexOf(item) === index));
    return dataBase 
}
let clients = mergeData(clients1,clients2);
console.log(clients);




// Завдання 2

// const characters = [
//     {
//       name: "Елена",
//       lastName: "Гилберт",
//       age: 17, 
//       gender: "woman",
//       status: "human"
//     },
//     {
//       name: "Кэролайн",
//       lastName: "Форбс",
//       age: 17,
//       gender: "woman",
//       status: "human"
//     },
//     {
//       name: "Аларик",
//       lastName: "Зальцман",
//       age: 31,
//       gender: "man",
//       status: "human"
//     },
//     {
//       name: "Дэймон",
//       lastName: "Сальваторе",
//       age: 156,
//       gender: "man",
//       status: "vampire"
//     },
//     {
//       name: "Ребекка",
//       lastName: "Майклсон",
//       age: 1089,
//       gender: "woman",
//       status: "vempire"
//     },
//     {
//       name: "Клаус",
//       lastName: "Майклсон",
//       age: 1093,
//       gender: "man",
//       status: "vampire"
//     }
//   ];


//   const charactersShortInfo = characters.map(function(el) {
//      let {name, lastName, age} = el;
//      return el = {name, lastName, age};
//   })
//   console.log(charactersShortInfo);


//   Завдання 3

// const user1 = {
//     name: "John",
//     years: 30,
//   };

//   let {name, years: age, isAdmin = false } = user1;
//   console.log(name, age, isAdmin);

// Завдання 4

// const satoshi2020 = {
//     name: 'Nick',
//     surname: 'Sabo',
//     age: 51,
//     country: 'Japan',
//     birth: '1979-08-21',
//     location: {
//       lat: 38.869422, 
//       lng: 139.876632
//     }
//   }
  
//   const satoshi2019 = {
//     name: 'Dorian',
//     surname: 'Nakamoto',
//     age: 44,
//     hidden: true,
//     country: 'USA',
//     wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
//     browser: 'Chrome'
//   }
  
//   const satoshi2018 = {
//     name: 'Satoshi',
//     surname: 'Nakamoto', 
//     technology: 'Bitcoin',
//     country: 'Japan',
//     browser: 'Tor',
//     birth: '1975-04-05'
//   }


//   const satoshi = {...satoshi2018,...satoshi2019,...satoshi2020};
//   console.log(satoshi);

// Завдання 5

// const books = [{
//     name: 'Harry Potter',
//     author: 'J.K. Rowling'
//   }, {
//     name: 'Lord of the rings',
//     author: 'J.R.R. Tolkien'
//   }, {
//     name: 'The witcher',
//     author: 'Andrzej Sapkowski'
//   }];
  
//   const bookToAdd = {
//     name: 'Game of thrones',
//     author: 'George R. R. Martin'
//   }

//   let books1 = [...books,...[bookToAdd]];

// console.log(books1);

// // Завдання 6

// const employee = {
//     name: 'Vitalii',
//     surname: 'Klichko'
//   }

//   const {name, surname, age = 0, salary = 0} = employee;
//   const employee1 = {name, surname, age, salary};
//   console.log(employee1);


// Завдання 7

// const array = ['value', () => 'showValue'];

// const [value, showValue] = array;

// alert(value); // має бути виведено 'value'
// alert(showValue());  // має бути виведено 'showValue'



