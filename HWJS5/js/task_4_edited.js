"use strict"

function helper(productsString) {
    const warehouse = "water,banana,black,tea,apple";
    let stringToReturn = "";
    const productsArray = productsString.split(",");
    for (const product of productsArray) {
        if (warehouse.includes(product.toLowerCase())) {
            stringToReturn += `${product} : ${Math.random()};\n`;
        } else {
            stringToReturn += `${product} : not found;\n`;
        }
    }
    return stringToReturn;
}

console.log(helper("WATER,banana,tea,APPLE,mango,fish"));