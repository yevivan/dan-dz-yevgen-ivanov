"use strict"

const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    },
  
  ];

let divContainer = document.querySelector("#root");
let bookList = document.createElement("ul");
divContainer.append(bookList);

  class BookCard {
      constructor({author, name, price}) {
        this.author = author;
        this.name = name;
        this.price = price;
      }

      createCards () {
          this.listItem = document.createElement("li");
          bookList.append(this.listItem);
          bookList.classList.add('book-list');
          this.listItem.classList.add('book-list__item');
          this.listItem.innerText = `${this.author};
          ${this.name};
          ${this.price} UAH; `

      }
  }

let ErrorPrice = new Error(`Стоимость не определена в карточке №:`);
let ErrorAuthor = new Error(`Автор не определен в карточке №:`)
let ErrorName = new Error(`Название не определено в карточке №:`)


books.forEach((element, index) => {
    try {
    if (element.name !== undefined && element.author !== undefined && element.price !== undefined) {
        new BookCard (element).createCards();
    }
    else if (element.price === undefined) {
        throw ErrorPrice;
    }  
    else if (element.name === undefined) {
        throw ErrorName;
    }  
    else if (element.author === undefined) {
        throw ErrorAuthor;
    }  

}   catch(e) {
    console.error(e + `${index + 1}`);}
});
  