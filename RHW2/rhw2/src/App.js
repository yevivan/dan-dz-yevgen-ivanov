import { Component } from "react";
import styles from './App.module.scss';
import Header from './components/Header';
// import Preloader from './components/Preloader';
import Modal from './components/Modal/Modal';
import CardContainer from "./components/Card container/CardContainer";
import CartContainer from "./components/CartContainer/CartContainer";
import ModalAddtocart from "./components/Modal/ModalAddTocart";

class App extends Component {
    state = {
        cards: [],
        areCardsLoading: true,
        carts: [],
        favourites: [],
        isOpenModal: false,
        isOpenModalAddTocart: false,
        modalProps: {},
        addTocartModalProps:{},
       }

    addToCart = (card) => {
        this.setState((current) => {
            const carts = [...current.carts]

            const index = carts.findIndex(el => el.id === card.id)

            if (index === -1) {
                carts.push({...card, count: 1 })
                console.log(carts);
            } else {
                carts[index].count += 1
            }

            localStorage.setItem("carts", JSON.stringify(carts))

            // console.log(carts);
            return { carts}
            
        })
    }

    
     async componentDidMount() {
         let cards = await fetch(`items.json`, {
             headers: {
                 'Content-Type': 'application/json',
                 'Accept': 'application/json'
             }
         }
       ).then(res => res.json()).then(data => data)
        //  console.log(cards);
         this.setState({ cards: cards });
         const carts = JSON.parse(localStorage.getItem("carts"))
         this.setState({carts:[...carts]})

         
         this.displayHeartOnfavotite(cards)
           
    }

    displayHeartOnfavotite = (cards) => {
        cards = cards.map(el => {
            const fovoriteCards = JSON.parse(localStorage.getItem("favourites"))
            this.setState({favourites: fovoriteCards})
            fovoriteCards.forEach(element => {
                if (el.id === element.id) {
                    el.isFavorite = true
                }
            })
            return el
            });
            this.setState({cards: cards})
    }
    
    incrementCartItem = (id) => {
        this.setState((current) => {
            const carts = [...current.carts]

            const index = carts.findIndex(el => el.id === id)

            if (index !== -1) {
                carts[index].count += 1
            }

            localStorage.setItem("carts", JSON.stringify(carts))
            // console.log(JSON.parse(localStorage.getItem("carts")));
            return { carts }
        })
    }

    dicrementCartItem = (id) => {
        this.setState((current) => {
            const carts = [...current.carts]

            const index = carts.findIndex(el => el.id === id)

            if (index !== -1 && carts[index].count > 1) {
                carts[index].count -= 1
            }

            localStorage.setItem("carts", JSON.stringify(carts))
            return { carts }
        })
    }

    deleteCartItem = (id) => {
        this.setState((current) => {
            const carts = [...current.carts]

            const index = carts.findIndex(el => el.id === id)

            if (index !== -1) {
                carts.splice(index, 1);
            }

            localStorage.setItem("carts", JSON.stringify(carts))
            return { carts }
        })
    }

       toggleModal = (value) => {
        this.setState({ isOpenModal: value })
    }

    toggleAddtocartModal = (value) => {
        this.setState({ isOpenModalAddTocart: value })
    }

         setModalProps = (value) => {
        this.setState({ modalProps: value })
    }

       setAddTocartModalProps = (card) => {
        this.setState({ addTocartModalProps: card })
       }


    addToFavorites = (card) => {
        if (!card.isFavorite) {
            this.setState((current) => {
                const favourites = [...current.favourites];
                favourites.push({ ...card, isFavorite: true  })
                const cards = current.cards.map((el => { 
                   if (el.id === card.id) {
                        el.isFavorite = true
                   }
                   return el
               }))
                localStorage.setItem("favourites", JSON.stringify(favourites))

                console.log(favourites);
                return { favourites, cards: cards }
            })
        }
        else {
            this.setState((current) => {
                const favourites = [...current.favourites];
                const index = favourites.findIndex(el => el.id === card.id)
                favourites.splice(index, 1)
                const cards = current.cards.map((el => { 
                   if (el.id === card.id) {
                        el.isFavorite = false
                   }
                   return el
               }))
                localStorage.setItem("favourites", JSON.stringify(favourites))

                console.log(favourites);
                return { favourites, cards: cards }
            })
        }
         
    }
       

    render() {
        
        const { cards, carts, isOpenModal, modalProps, favourites, isOpenModalAddTocart, addTocartModalProps } = this.state


        return (
            <div className={styles.App}>
                
                <Header itemsCount={carts.length} fovoritesCount={favourites.length}/>
                <main>

                    <section className={styles.leftContainer}>
                        <h1>Available items</h1>
                        <CardContainer toggleAddtocartModal={this.toggleAddtocartModal}  cards={cards} addToCart={this.addToCart} addToFavorites={this.addToFavorites } setAddTocartModalProps = {this.setAddTocartModalProps} />
                    </section>

                    <section className={styles.rightContainer}>
                        <h2>Cart</h2>
                           <CartContainer setModalProps={this.setModalProps} toggleModal={this.toggleModal} incrementCartItem={this.incrementCartItem} dicrementCartItem={this.dicrementCartItem} carts={carts} />
                    </section>
                </main>
                <Modal modalProps={modalProps} deleteCartItem={this.deleteCartItem} isOpen={isOpenModal} toggleModal={this.toggleModal} />
                <ModalAddtocart addTocartModalProps={addTocartModalProps} addToCart={this.addToCart} isOpenModalAddTocart={isOpenModalAddTocart} toggleAddtocartModal={this.toggleAddtocartModal} />
            </div>
        );
    }
}


export default App;


