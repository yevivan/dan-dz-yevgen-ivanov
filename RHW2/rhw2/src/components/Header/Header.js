import {PureComponent} from 'react';
import styles from './Header.module.scss';
import cartIcon from '../../svg/cart-outline.svg'
import heartIcon from '../../svg/heart.svg'
import PropTypes from 'prop-types';

class Header extends PureComponent {

    render() {
        const {itemsCount, fovoritesCount} = this.props
        return (
            <header className={styles.header}>
                <div>
                </div>

                {/* Centered navigation */}

                                   {/* <img
      src="https://everysong.pro/static/media/logo.646b1e00.svg"
      alt="logo"
    /> */}
              
                <nav>
                    <ul>       

                        <li>
                            <a href="">Music Shop</a>
                        </li>  
                    </ul>
                </nav>

                {/* Right container */}
                    <div className= {styles.cardContainer}>
                        <ul>
                            <li>
                                <a href="#"><img src={cartIcon} alt="Cart" /></a>
                            </li>
                        </ul>
                    <span>{itemsCount }</span>
                      <ul>
                     <li>
                                <a href="#"><img src={heartIcon} alt="heart" /></a>
                            </li>
                        </ul>
                    <span>{fovoritesCount }</span>
                    </div>
            </header>
        );
    }
}
   
export default Header;

Header.propTypes = {
    itemsCount: PropTypes.number,
    fovoritesCount: PropTypes.number
};



Header.defaultProps = {
    itemsCount: 0,
    fovoritesCount: 0,
};

