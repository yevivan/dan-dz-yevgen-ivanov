import { PureComponent } from 'react';
import styles from './CardItem.module.scss';
import Button from '../Button';
import heartSvg from '../../svg/heart.svg';
import outlineHeartSvg from '../../svg/heart-outline.svg'
import PropTypes from 'prop-types';

class CardItem extends PureComponent {
     
    render() {
        const { addToCart,setAddTocartModalProps, addToFavorites, title, img, description, isFavorite, id, price, toggleAddtocartModal, thisCard} = this.props;
        // console.log(isFavorite);
       
      
       
        // const favoriteIcon = isFavorite ? outlineHeartSvg : heartSvg;
        // console.log(favoriteIcon);
        return (
            <div className={styles.card}>
                <button type="button" className={styles.likeButton} onClick={() => {
                    addToFavorites({ title, isFavorite, id })
                    console.log(isFavorite);
                }}>
                    <img src={isFavorite ? heartSvg : outlineHeartSvg} alt="Favourite" />
                 
                </button>
                
                
                    <img className={styles.itemAvatar} src={img}
                        alt={title} />
                    <span className={styles.title}>{title}</span>
                    <span className={styles.description}>{description}</span>

                    <div className={styles.btnContainer}>
                        <span className={styles.price}>{price}</span>
                    <Button 
                  
                    onClick={() => { 
                        toggleAddtocartModal(true)
                        setAddTocartModalProps(thisCard)
                        console.log(thisCard);
                    }}  
                    >Add to cart</Button>
                        </div>
                    
            </div>
        )
    }
}

export default CardItem;

CardItem.propTypes = {
    addToCart: PropTypes.func,
	cards: PropTypes.array,
    addToFavorites: PropTypes.func,
    title: PropTypes.string,
    description: PropTypes.string,
    isFavorite: PropTypes.bool,
    id: PropTypes.number,
    price: PropTypes.string,
};



CardItem.defaultProps = {
    cards: undefined,
    title: "Item",
    description: "Good song",
    isFavorite: false,
    price: 0,
};