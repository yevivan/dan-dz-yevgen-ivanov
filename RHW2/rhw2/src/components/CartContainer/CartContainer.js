import {PureComponent} from 'react';
import CartItem from '../CartItem/';
import PropTypes from 'prop-types';

class CartContainer extends PureComponent {

render(){

    const { carts, incrementCartItem, dicrementCartItem, toggleModal, setModalProps } = this.props

    return(
        <ul>
{carts.map(({title, img, count, id }) => {

return <CartItem setModalProps={setModalProps} toggleModal={toggleModal} title={title} img={img} count={count} id={id} incrementCartItem={incrementCartItem} dicrementCartItem={dicrementCartItem} />
})}
        </ul>
    )
}

}

export default CartContainer;

CartContainer.propTypes = {
	carts: PropTypes.array,
    incrementCartItem: PropTypes.func,
    
};

