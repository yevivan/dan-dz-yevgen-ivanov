import React, { PureComponent } from 'react';
import CardItem from '../Card/CardItem';
import styles from './CardContainer.module.scss';
import PropTypes from 'prop-types';

class CardContainer extends PureComponent {
	render() {
		
		const { addToCart, cards, addToFavorites, toggleAddtocartModal, setAddTocartModalProps} = this.props;
		
		return (
			<div>
				<ul className={styles.list}>
					{cards.map((card) => (
						//   console.log(title, img, description, isFavorite, id);
							
						<li key={card.id}>					
							<CardItem
								addToFavorites={addToFavorites}
								addToCart={addToCart}
								id={card.id}
								title={card.title}
								description={card.description}
								img={card.img}
								isFavorite={card.isFavorite}
								price = {card.price}
							
								thisCard={card}
								toggleAddtocartModal = {toggleAddtocartModal}
								setAddTocartModalProps = {setAddTocartModalProps}
								/>
						</li>
					 ))}
				</ul>
			</div>
		);
	}
}

export default CardContainer;

CardContainer.propTypes = {
    addToCart: PropTypes.func,
	cards: PropTypes.array,
	addToFavorites: PropTypes.func,
};



CardContainer.defaultProps = {
    cards: undefined,
};