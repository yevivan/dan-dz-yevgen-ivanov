"use strict"



function getNumber (enteredValue = 0) {
    let insertedNumber = prompt("Please enter a number", enteredValue)
    if ((isNaN(insertedNumber) || insertedNumber == "" || insertedNumber == " " || insertedNumber == null || insertedNumber.trim() == '') || insertedNumber <= 0)
    {return getNumber(insertedNumber) 
     }
    return insertedNumber

}


function calculateFactorial(number) {
    if (number == 1) return 1
    return number*calculateFactorial(number - 1)
}

let result = calculateFactorial(getNumber())
console.log(result);





