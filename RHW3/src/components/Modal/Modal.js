import React from "react";
import styles from "./modal.module.scss";
import PropTypes, { object } from 'prop-types';

const Modal = (props) => {
 
        const { isOpen, toggleModal, deleteCartItem, modalProps } = props;
        if (!isOpen) {
            return null;
        }
        
            return <>
                <div className={styles.darkBG} onClick={() => toggleModal(false)} />
                <div className={styles.centered}>
                    <div className={styles.modal}>
                        <div className={styles.modalHeader}>
                            <h5 className={styles.heading}>Deleting item from cart</h5>
                            <button className={styles.closeBtn} onClick={() => toggleModal(false)}>X
                            </button>
                         
                        </div>
                        <div className={styles.modalContent} >
                            Are you sure to delete {modalProps.title}?
                            <div className={styles.modalActions} >
                                <div className={styles.actionsContainer}>
                                    <button className={styles.deleteBtnBlue} onClick={() => toggleModal(false)} >Cancel</button>
                                    <button className={styles.cancelBtnBlue} onClick={() => {
                            deleteCartItem(modalProps.id);
                            toggleModal(false);
                        }}>Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
            </>

        
    
}

Modal.propTypes = {
    onClick: PropTypes.func,
    isOpen: PropTypes.bool,
    toggleModal: PropTypes.func,
    className: PropTypes.string,
    modalProps: object,

};

Modal.defaultProps = {
    isOpen: true,
    className: '',

};

export default Modal