import CardContainer from "../Card container/CardContainer";
import ModalAddtocart from "../Modal/ModalAddTocart";
import styles from '../Pages/Pages.module.scss';
import PropTypes from 'prop-types';




const FavoritiesPage = (props) => {
    const {addToCart, cards, addToFavorites, toggleAddtocartModal, setAddTocartModalProps, addTocartModalProps, isOpenModalAddTocart, showOnlyFavorities} = props;
    return (
      <>
            <main>
                <section className={styles.leftContainer}>
                    <h1>Your favorities</h1>
                    <CardContainer toggleAddtocartModal={toggleAddtocartModal}  cards={cards} addToFavorites={addToFavorites } setAddTocartModalProps = {setAddTocartModalProps} showOnlyFavorities = {showOnlyFavorities} />
                </section>
            </main>
            <ModalAddtocart addTocartModalProps={addTocartModalProps} addToCart={addToCart} isOpenModalAddTocart={isOpenModalAddTocart} toggleAddtocartModal={toggleAddtocartModal} />
      
            </>
    );

}

FavoritiesPage.propTypes = {
    addToCart: PropTypes.func,
    carts: PropTypes.array,
    cards: PropTypes.array,
    toggleAddtocartModal: PropTypes.func,
    setAddTocartModalProps: PropTypes.func,
    showOnlyFavorities: PropTypes.bool,

};

FavoritiesPage.defaultProps = {
    cards: [],

};


export default FavoritiesPage;