
import { Routes, Route,} from 'react-router-dom';
import HomePage from "./components/Pages/HomePage";
import CartPage from "./components/Pages/CartPage"
import FavoritiesPage from "./components/Pages/FavoritiesPage"


const AppRoutes = (props) => {

    const {addToCart, 
        cards, 
        addToFavorites, 
        toggleAddtocartModal, 
        setAddTocartModalProps, 
        addTocartModalProps, 
        isOpenModalAddTocart,
        carts, 
        incrementCartItem, 
        dicrementCartItem, 
        toggleModal, 
        setModalProps,
        modalProps,
        deleteCartItem,
        isOpenModal,
        showOnlyFavorities
            } = props;

  
    return (
        <Routes>
            <Route path='/' element={<HomePage 
            showOnlyFavorities = {showOnlyFavorities}
             cards={cards} 
             addToCart={addToCart} 
             addToFavorites={addToFavorites } 
             setAddTocartModalProps = {setAddTocartModalProps} 
             addTocartModalProps={addTocartModalProps} 
             isOpenModalAddTocart={isOpenModalAddTocart} 
             toggleAddtocartModal={toggleAddtocartModal}
             />} />
            <Route path='/cart' element={<CartPage
            setModalProps={setModalProps} 
            toggleModal={toggleModal} 
            incrementCartItem={incrementCartItem}
             dicrementCartItem={dicrementCartItem} 
             carts={carts}
             modalProps={modalProps} 
            deleteCartItem={deleteCartItem} 
            isOpenModal={isOpenModal} 
            />} />
            <Route path='/favorities' element={<FavoritiesPage
             showOnlyFavorities = {showOnlyFavorities}
             cards={cards} 
             addToCart={addToCart} 
             addToFavorites={addToFavorites } 
             setAddTocartModalProps = {setAddTocartModalProps} 
             addTocartModalProps={addTocartModalProps} 
             isOpenModalAddTocart={isOpenModalAddTocart} 
             toggleAddtocartModal={toggleAddtocartModal}
             />} />
        </Routes>
    )
}
export default AppRoutes;