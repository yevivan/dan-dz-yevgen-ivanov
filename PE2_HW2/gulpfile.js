import gulp from 'gulp';
import browsersync  from  'browser-sync';
import gulpautoprefixer  from  'gulp-autoprefixer';
import gulpclean  from  'gulp-clean';
import gulpcleancss  from  'gulp-clean-css';
import gulpconcat  from  'gulp-concat';
import gulpimagemin  from  'gulp-imagemin';
import gulpminify  from  'gulp-minify';
import gulpSass  from  'gulp-sass';
import gulpuglify  from  'gulp-uglify';
import dartSass  from  'sass';
import gulphtmlmin from 'gulp-htmlmin';
import del from "del";
const BS = browsersync.create();
const sass = gulpSass(dartSass);

export const buildStyles = () => gulp.src('./src/sass/**/*.scss')
            .pipe(sass())
            .pipe( gulpautoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {
                cascade: false
             }))
            .pipe(gulp.dest('./dist/'));
            

export const mynifyhtml = () => gulp.src('./*.html')
            .pipe(gulphtmlmin({collapseWhitespace: true}))     
            .pipe(gulp.dest('./dist/'));


export const imageMin = () => gulp.src('./src/img/**/*')
       .pipe(gulpimagemin())
       .pipe(gulp.dest("./dist/images"))


export const build = gulp.series (buildStyles, mynifyhtml, (done) => {
    done();
});


export const dev = gulp.series (build, () => {
    BS.init({
        server: {
           baseDir: "./"
        }
    });

    gulp.watch(['./src/**', './*.html'], gulp.series(build, (done) => {
        BS.reload();
        done();
    }))
});


