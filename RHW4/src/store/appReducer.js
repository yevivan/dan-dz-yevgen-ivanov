import { combineReducers } from "redux";
import getcardsReducer from "./Getting cards/GetCardsReducer";
import addToCartreducer from "./Add to cart/AddToCartReducer";

const appReducer = combineReducers({
  getCards: getcardsReducer,
  addToCart: addToCartreducer,
});

export default appReducer;
