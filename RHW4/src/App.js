import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCards, addToFavorites } from "./store/Getting cards/ActionsCreator";
import {
  addToCart,
  incrementCartItem,
  dicrementCartItem,
  deleteCartItem,
  setAddTocartModalProps,
  toggleAddtocartModal,
  setModalProps,
  toggleModal,
} from "./store/Add to cart/ActionsCreator";
import styles from "./App.module.scss";
import AppRoutes from "./AppRoutes";
import Header from "./components/Header/Header";

const App = () => {
  const dispatch = useDispatch();
  const cards = useSelector((store) => store.getCards.cards);
  const favourites = useSelector((store) => store.getCards.favourites);
  const carts = useSelector((store) => store.addToCart.carts);
  const addTocartModalProps = useSelector(
    (store) => store.addToCart.addTocartModalProps
  );
  const isOpenModalAddTocart = useSelector(
    (store) => store.addToCart.isOpenModalAddTocart
  );
  const modalProps = useSelector((store) => store.addToCart.modalProps);
  const isOpenModal = useSelector((store) => store.addToCart.isOpenModal);

  useEffect(() => {
    dispatch(getCards());
  }, [dispatch]);

  return (
    <div className={styles.App}>
      <Header itemsCount={carts.length} fovoritesCount={favourites.length} />
      <section>
        <AppRoutes
          cards={cards}
          addToCart={addToCart}
          addToFavorites={addToFavorites}
          setAddTocartModalProps={setAddTocartModalProps}
          addTocartModalProps={addTocartModalProps}
          isOpenModalAddTocart={isOpenModalAddTocart}
          toggleAddtocartModal={toggleAddtocartModal}
          setModalProps={setModalProps}
          toggleModal={toggleModal}
          incrementCartItem={incrementCartItem}
          dicrementCartItem={dicrementCartItem}
          carts={carts}
          modalProps={modalProps}
          deleteCartItem={deleteCartItem}
          isOpenModal={isOpenModal}
          showOnlyFavorities={true}
        />
      </section>
    </div>
  );
};

export default App;
