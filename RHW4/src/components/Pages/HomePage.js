import CardContainer from "../Card container/CardContainer";
import ModalAddtocart from "../Modal/ModalAddTocart";
import styles from "../Pages/Pages.module.scss";
import PropTypes from "prop-types";

const HomePage = (props) => {
  const {
    addToCart,
    cards,
    addToFavorites,
    toggleAddtocartModal,
    setAddTocartModalProps,
    addTocartModalProps,
    isOpenModalAddTocart,
  } = props;

  return (
    <>
      <main>
        <section className={styles.leftContainer}>
          <h1>Available items</h1>
          <CardContainer
            toggleAddtocartModal={toggleAddtocartModal}
            cards={cards}
            addToFavorites={addToFavorites}
            setAddTocartModalProps={setAddTocartModalProps}
          />
        </section>
      </main>
      <ModalAddtocart
        addTocartModalProps={addTocartModalProps}
        addToCart={addToCart}
        isOpenModalAddTocart={isOpenModalAddTocart}
        toggleAddtocartModal={toggleAddtocartModal}
      />
    </>
  );
};

HomePage.propTypes = {
  addToCart: PropTypes.func,
  carts: PropTypes.array,
  cards: PropTypes.array,
  toggleAddtocartModal: PropTypes.func,
  setAddTocartModalProps: PropTypes.func,
  showOnlyFavorities: PropTypes.bool,
};

HomePage.defaultProps = {
  cards: [],
};

export default HomePage;
