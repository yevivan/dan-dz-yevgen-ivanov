import PropTypes from "prop-types";
import CartContainer from "../CartContainer/CartContainer";
import styles from "../Pages/Pages.module.scss";
import Modal from "../Modal/Modal";

const CartPage = (props) => {
  const {
    carts,
    incrementCartItem,
    dicrementCartItem,
    toggleModal,
    setModalProps,
    modalProps,
    deleteCartItem,
    isOpenModal,
  } = props;
  return (
    <main>
      <section className={styles.rightContainer}>
        <h2>Cart</h2>
        <CartContainer
          setModalProps={setModalProps}
          toggleModal={toggleModal}
          incrementCartItem={incrementCartItem}
          dicrementCartItem={dicrementCartItem}
          carts={carts}
        />
      </section>
      <Modal
        modalProps={modalProps}
        deleteCartItem={deleteCartItem}
        isOpen={isOpenModal}
        toggleModal={toggleModal}
      />
    </main>
  );
};

CartPage.propTypes = {
  incrementCartItem: PropTypes.func,
  carts: PropTypes.array,
  dicrementCartItem: PropTypes.func,
  toggleModal: PropTypes.func,
  setModalProps: PropTypes.func,
};

CartPage.defaultProps = {
  carts: [],
};

export default CartPage;
