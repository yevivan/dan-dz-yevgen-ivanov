import React from "react";
import styles from "./modal.module.scss";
import { useDispatch} from 'react-redux';

const ModalAddtocart = (props) => {
    const dispatch = useDispatch();
    
        const { isOpenModalAddTocart,  addToCart, addTocartModalProps, toggleAddtocartModal,  } = props;
        if (!isOpenModalAddTocart) {
            return null;
        }
       
            return <>
           
                <div className={styles.darkBG} onClick={() => dispatch(toggleAddtocartModal(false))} />
                <div className={styles.centered}>
                    <div className={styles.modal}>
                        <div className={styles.modalHeader}>
                            <h5 className={styles.heading}>Adding item from cart</h5>
                            <button className={styles.closeBtn} onClick={() => dispatch(toggleAddtocartModal(false))}>X
                            </button>
                         
                        </div>
                        <div className={styles.modalContent} >
                            Are you sure to add {addTocartModalProps.title}  to cart ?
                            <div className={styles.modalActions} >
                                <div className={styles.actionsContainer}>
                                    <button className={styles.deleteBtnBlue} onClick={() => dispatch(toggleAddtocartModal(false))} >Cancel</button>
                                    <button className={styles.cancelBtnBlue} onClick={() => {
                            dispatch(addToCart(addTocartModalProps))
                            dispatch(toggleAddtocartModal(false));
                        }}>Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
            </>

        
    
}

export default  ModalAddtocart