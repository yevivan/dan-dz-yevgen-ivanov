"use strict"

const usersUrl = "https://ajax.test-danit.com/api/json/users";
const postdUrl = `https://ajax.test-danit.com/api/json/posts`;
const cardsContainer = document.querySelector(".twitter")



class Card  {
            constructor (name, email, title, post, id ) {
                this.name = name;
                this.email = email;
                this.title = title;
                this.post = post;
                this.id = id;
                this.card = document.createElement('div')
                this.deleteButton = document.createElement('button')
            }
            render (selector) {
                this.card.className = 'card';
                this.deleteButton.className = 'btn-delete';
                this.deleteButton.innerText = 'DELETE'
                document.querySelector(selector).append(this.card)
                this.card.insertAdjacentHTML("beforeend", 
                `     <span class="user-name">${this.name}</span>
                    <span class="user-name e-mail">${this.email}</span>
                    <h2 class="card__header">${this.title}</h2>
                    <p class="card__text">
                    ${this.post}
                    </p>
                `)
                this.card.append(this.deleteButton);
                this.deleteCard();
            }  

            deleteCard () {
                this.deleteButton.addEventListener('click', (e) => {
                    deleteCardOnServer(this.id, this.card)  
                })
            }
   };

    const createUserPostsArray = () => {
    const usersPromise = fetch(usersUrl).then(response => response.json());
    const postsPromise = fetch(postdUrl).then(response => response.json());

    Promise.allSettled([usersPromise, postsPromise]).then((results) => {
        console.log(results);
        let usersArray = results[0].value;
        let postsArray = results[1].value;
    
           postsArray.forEach(el => {
                 usersArray.forEach(({id, name, email}) => {
                     if (el.userId === id) {
                        el.name = name;
                        el.email = email;
                     }
                 })
           })
           return postsArray
    })
    .then((usersAndPostArray) => {
        usersAndPostArray.forEach(({name, email, title, body: post, id}) => {
            new Card (name, email, title, post, id).render(".twitter")
           
             })
            

    })
    
    .catch (error => {
        alert("Something wrong111:   " + error.message)
    })

}


createUserPostsArray();
const deleteCardOnServer = ((cardId, card) => {
    fetch(`${postdUrl}/${cardId}`, {
        method: `DELETE`
    })
   
    .then ((response) => {
        if (response.status === 200) {
        console.log(response)
        card.remove();
    } 
    })
    .catch (error => {              /*Не ловится эта ошибка */
        alert("Something wrong:   " + error.message)
    })
    })

 

