"use strict"

const getIpBtn = document.querySelector('.get-ip');
const getAddressBtn = document.querySelector('.get-address');
const APIGeiIp = `https://api.ipify.org/?format=json`;
const APIGetAdress = `http://ip-api.com/`

const myIP = async () => {
    let MyIp = await fetch(APIGeiIp).then(response => response.json());
    console.log(MyIp);
    return MyIp.ip
}

    getIpBtn.addEventListener('click', async () => {
        document.querySelector('.show-ip').innerText = `Your IP:  ` + await myIP();
       
    });


const getMyAdress = async () => {
    const UrlWithMyIp = APIGetAdress + `json/${await myIP()}`
    console.log(UrlWithMyIp);
    const myAddress = await fetch(UrlWithMyIp)
    .then((response)  => response.json())
    console.log(myAddress);
    return  `My rontry:   ${myAddress.country} 
           My city:      ${myAddress.city},
            My region:      ${myAddress.region},
            My region name:    ${myAddress.regionName}`
    
    
}



getAddressBtn.addEventListener('click', async () => {
     console.log(1233);
        document.querySelector('.show-address').innerText = await getMyAdress();
       
    });