import React from "react";
import styles from "./modal.module.scss";

const ModalAddtocart = (props) => {
    
        const { isOpenModalAddTocart,  addToCart, addTocartModalProps, toggleAddtocartModal,  } = props;
        if (!isOpenModalAddTocart) {
            return null;
        }
        
            return <>
           
                <div className={styles.darkBG} onClick={() => toggleAddtocartModal(false)} />
                <div className={styles.centered}>
                    <div className={styles.modal}>
                        <div className={styles.modalHeader}>
                            <h5 className={styles.heading}>Adding item from cart</h5>
                            <button className={styles.closeBtn} onClick={() => toggleAddtocartModal(false)}>X
                            </button>
                         
                        </div>
                        <div className={styles.modalContent} >
                            Are you sure to add {addTocartModalProps.title}  to cart ?
                            <div className={styles.modalActions} >
                                <div className={styles.actionsContainer}>
                                    <button className={styles.deleteBtnBlue} onClick={() => toggleAddtocartModal(false)} >Cancel</button>
                                    <button className={styles.cancelBtnBlue} onClick={() => {
                            addToCart(addTocartModalProps);
                            console.log(addTocartModalProps);
                            toggleAddtocartModal(false);
                        }}>Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
            </>

        
    
}

export default  ModalAddtocart