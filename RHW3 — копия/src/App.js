import {  useEffect, useState } from "react";
import styles from './App.module.scss';
import AppRoutes from "./AppRoutes";
import Header from "./components/Header/Header";


const App = () => {

    const [cards, setCards] = useState([]);
    const [carts, setCarts] = useState([]);
    const [favourites, setFavorities] = useState([]);
    const [isOpenModal, setModalStatus] = useState(false);
    const [isOpenModalAddTocart, setModalAddToCartStatus] = useState(false);
    const [modalProps, setModalPropsHook] = useState({});
    const [addTocartModalProps, setAddTocartModalPropsHook] = useState({});
    
    const addToCart = (card) => {
    
            const cartsArr = [...carts]
            const index = cartsArr.findIndex(el => el.id === card.id)
            if (index === -1) {
                cartsArr.push({...card, count: 1 })
            } else {
                cartsArr[index].count += 1
            }
            localStorage.setItem("carts", JSON.stringify(cartsArr))
            setCarts (cartsArr)   
    }
    
    
            useEffect(() => {
                try {
                async function fetchcards() {
                let cards = await fetch(`items.json`, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    }
                }
            ).then(res => res.json()).then(data => data)
                setCards(cards);
                displayHeartOnfavotite(cards)
            };
            fetchcards() 
            } catch (err) {console.log(err);}
           
            
        }, []
        )

        useEffect(()=> {
            const cartsInLocalStorage = JSON.parse(localStorage.getItem("carts"))
            cartsInLocalStorage ? setCarts(cartsInLocalStorage) : setCarts(carts)

        }, [])
    
    
    const displayHeartOnfavotite = (cards) => {
        const fovoriteCards = JSON.parse(localStorage.getItem("favourites")) ? JSON.parse(localStorage.getItem("favourites")) : favourites
        setFavorities(fovoriteCards)
        cards = cards.map(el => { 
            fovoriteCards.forEach(element => {
                if (el.id === element.id) {
                    el.isFavorite = true
                }
            })
            return el
            });
            setCards(cards)
    }
    
    const addToFavorites = (card) => {
        if (!card.isFavorite) {
                const arrFavourites = [...favourites];
                arrFavourites.push({ ...card, isFavorite: true  })
                const arrCards = cards.map((el => { 
                   if (el.id === card.id) {
                        el.isFavorite = true
                   }
                   return el
               }))
                localStorage.setItem("favourites", JSON.stringify(arrFavourites))
                setFavorities(arrFavourites)
                setCards(arrCards)
        }
        else {
                const arrFavourites = [...favourites];
                const index = arrFavourites.findIndex(el => el.id === card.id)
                arrFavourites.splice(index, 1)
                const arrCards = cards.map((el => { 
                   if (el.id === card.id) {
                        el.isFavorite = false
                   }
                   return el
               }))
                localStorage.setItem("favourites", JSON.stringify(arrFavourites))
                setFavorities(arrFavourites);
                setCards(arrCards)
        }    
    }

    const incrementCartItem = (id) => {
            const arrCarts = [...carts]
            const index = arrCarts.findIndex(el => el.id === id)
            if (index !== -1) {
                arrCarts[index].count += 1
            }
            localStorage.setItem("carts", JSON.stringify(arrCarts))
            setCarts(arrCarts)  
          
    }


    
    
    const dicrementCartItem = (id) => {
            const arrCarts = [...carts]
            const index = arrCarts.findIndex(el => el.id === id)
            if (index !== -1 && arrCarts[index].count > 1) {
                arrCarts[index].count -= 1
            }
            localStorage.setItem("carts", JSON.stringify(arrCarts))
           setCarts(arrCarts)
    }
    
    const deleteCartItem = (id) => {
            const cartsArr = [...carts]
            const index = cartsArr.findIndex(el => el.id === id)
            if (index !== -1) {
                cartsArr.splice(index, 1);
            }
            localStorage.setItem("carts", JSON.stringify(cartsArr))
            setCarts(cartsArr)
    }
    
    const toggleModal = (value) => {
        setModalStatus(value)
    }
    
    const toggleAddtocartModal = (value) => {
        setModalAddToCartStatus(value)
    }
    
    const setModalProps = (value) => {
            setModalPropsHook(value)
    }
    
    const setAddTocartModalProps = (card) => {
        setAddTocartModalPropsHook(card)
       }
    
    
   
        return (
           
            <div className={styles.App}> 
             <Header itemsCount={carts.length} fovoritesCount={favourites.length}/>
            <section>
            <AppRoutes
            cards={cards} 
            addToCart={addToCart} 
            addToFavorites={addToFavorites } 
            setAddTocartModalProps = {setAddTocartModalProps} 
            addTocartModalProps={addTocartModalProps} 
            isOpenModalAddTocart={isOpenModalAddTocart} 
            toggleAddtocartModal={toggleAddtocartModal}
            setModalProps={setModalProps} 
            toggleModal={toggleModal} 
            incrementCartItem={incrementCartItem}
            dicrementCartItem={dicrementCartItem} 
            carts={carts}
            modalProps={modalProps} 
            deleteCartItem={deleteCartItem} 
            isOpenModal={isOpenModal} 
            showOnlyFavorities = {true}
            />
            </section>
            </div>
       
        );
}


export default App;

